package hu.braininghub.bh06.dao;

import java.util.List;

import javax.persistence.EntityManager;

import hu.braininghub.bh06.entity.Employee;

public class DefaultEmployeeDAO implements EmployeeDAO{

	private EntityManager entitymanager;
	
	public DefaultEmployeeDAO(EntityManager entitymanager) {
		super();
		this.entitymanager = entitymanager;
	}

	public void update(Employee entity) {
		entitymanager.getTransaction().begin();
		entitymanager.merge(entity);
		entitymanager.flush();
		entitymanager.getTransaction().commit();
	}

	public void create(Employee entity) {
		entitymanager.getTransaction().begin();
		entitymanager.persist(entity);
		entitymanager.getTransaction().commit();
	}

	public void delete(Employee entity) {
		entitymanager.getTransaction().begin();
		entitymanager.remove(entity);
		entitymanager.getTransaction().commit();		
		
	}
	
	public void deleteById(int id) {
		entitymanager.getTransaction().begin();
		Employee employee=getEmployeeById(id);
		entitymanager.remove(employee);
		entitymanager.getTransaction().commit();	
	}

	public List<Employee> getAll() {
		//entitymanager.createNamedQuery("Employee.findAll", Employee.class).getResultList();
		return entitymanager.createNamedQuery("Employee.findAll", Employee.class).getResultList();
	}

	public Employee getEmployeeById(int eid) {
		Employee searchedEmployee= entitymanager.find(Employee.class, eid);
		
		return searchedEmployee;
	}

	
}
