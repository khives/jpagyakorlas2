package hu.braininghub.bh06.dao;

import java.util.List;

import hu.braininghub.bh06.entity.Employee;

public interface EmployeeDAO {

	void update(Employee entity);
	void create(Employee entity);
	void delete(Employee entity);
	List<Employee> getAll();
	Employee getEmployeeById(int eid);
	void deleteById(int id);
}
