package hu.brainghub.bh06.app;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import hu.braininghub.bh06.dao.DefaultEmployeeDAO;
import hu.braininghub.bh06.dao.EmployeeDAO;
import hu.braininghub.bh06.entity.Employee;

public class App {

	public static void main(String[] args) {
		EntityManagerFactory entityManagerFacetory= Persistence.createEntityManagerFactory("essPU");
		EntityManager entityManager= entityManagerFacetory.createEntityManager();
		
		EmployeeDAO employeeDAO= new DefaultEmployeeDAO(entityManager);
		
		Employee trankus=new Employee();
//		trankus.setEid(17);
		trankus.setEname("Trankus");
		trankus.setSalary(160002.00);
		trankus.setDeg("deg2");
		

		employeeDAO.create(trankus);
		//employeeDAO.getAll();
		System.out.println("ez: ");
		System.out.println(employeeDAO.getEmployeeById(2).toString());
		
//		Employee torolni= employeeDAO.getEmployeeById(3);
//		employeeDAO.delete(torolni);
		
		employeeDAO.deleteById(5);
		
		entityManager.close();
		entityManagerFacetory.close();
	}
}
